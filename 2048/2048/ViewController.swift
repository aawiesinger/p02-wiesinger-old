//
//  ViewController.swift
//  2048
//
//  Created by Annika Wiesinger on 2/7/17.
//  Copyright © 2017 Annika Wiesinger. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var tile1: UILabel!
    @IBOutlet var tile2: UILabel!
    @IBOutlet var tile3: UILabel!
    @IBOutlet var tile4: UILabel!
    @IBOutlet var tile5: UILabel!
    @IBOutlet var tile6: UILabel!
    @IBOutlet var tile7: UILabel!
    @IBOutlet var tile8: UILabel!
    @IBOutlet var tile9: UILabel!
    @IBOutlet var tile10: UILabel!
    @IBOutlet var tile11: UILabel!
    @IBOutlet var tile12: UILabel!
    @IBOutlet var tile13: UILabel!
    @IBOutlet var tile14: UILabel!
    @IBOutlet var tile15: UILabel!
    @IBOutlet var tile16: UILabel!
    
    var tiles: [[Int]] = [[0,0,0,0],
                          [0,0,0,0],
                          [0,0,0,0],
                          [0,0,0,0]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tile1.text = String(tiles[0][0])
        tile2.text = String(tiles[0][1])
        tile3.text = String(tiles[0][2])
        tile4.text = String(tiles[0][3])
        tile5.text = String(tiles[1][0])
        tile6.text = String(tiles[1][1])
        tile7.text = String(tiles[1][2])
        tile8.text = String(tiles[1][3])
        tile9.text = String(tiles[2][0])
        tile10.text = String(tiles[2][1])
        tile11.text = String(tiles[2][2])
        tile12.text = String(tiles[2][3])
        tile13.text = String(tiles[3][0])
        tile14.text = String(tiles[3][1])
        tile15.text = String(tiles[3][2])
        tile16.text = String(tiles[3][3])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func upButton(sender: AnyObject) {
        updateBoard(tiles, direction: "up")
        tile1.text = String(tiles[0][0])
        tile2.text = String(tiles[0][1])
        tile3.text = String(tiles[0][2])
        tile4.text = String(tiles[0][3])
        tile5.text = String(tiles[1][0])
        tile6.text = String(tiles[1][1])
        tile7.text = String(tiles[1][2])
        tile8.text = String(tiles[1][3])
        tile9.text = String(tiles[2][0])
        tile10.text = String(tiles[2][1])
        tile11.text = String(tiles[2][2])
        tile12.text = String(tiles[2][3])
        tile13.text = String(tiles[3][0])
        tile14.text = String(tiles[3][1])
        tile15.text = String(tiles[3][2])
        tile16.text = String(tiles[3][3])
    }
    
    @IBAction func downButton(sender: AnyObject) {
        tile1.text = String(tiles[0][0])
        tile2.text = String(tiles[0][1])
        tile3.text = String(tiles[0][2])
        tile4.text = String(tiles[0][3])
        tile5.text = String(tiles[1][0])
        tile6.text = String(tiles[1][1])
        tile7.text = String(tiles[1][2])
        tile8.text = String(tiles[1][3])
        tile9.text = String(tiles[2][0])
        tile10.text = String(tiles[2][1])
        tile11.text = String(tiles[2][2])
        tile12.text = String(tiles[2][3])
        tile13.text = String(tiles[3][0])
        tile14.text = String(tiles[3][1])
        tile15.text = String(tiles[3][2])
        tile16.text = String(tiles[3][3])
    }
    
    @IBAction func leftButton(sender: AnyObject) {
        tile1.text = String(tiles[0][0])
        tile2.text = String(tiles[0][1])
        tile3.text = String(tiles[0][2])
        tile4.text = String(tiles[0][3])
        tile5.text = String(tiles[1][0])
        tile6.text = String(tiles[1][1])
        tile7.text = String(tiles[1][2])
        tile8.text = String(tiles[1][3])
        tile9.text = String(tiles[2][0])
        tile10.text = String(tiles[2][1])
        tile11.text = String(tiles[2][2])
        tile12.text = String(tiles[2][3])
        tile13.text = String(tiles[3][0])
        tile14.text = String(tiles[3][1])
        tile15.text = String(tiles[3][2])
        tile16.text = String(tiles[3][3])
    }
    
    @IBAction func rightButton(sender: AnyObject) {
        tile1.text = String(tiles[0][0])
        tile2.text = String(tiles[0][1])
        tile3.text = String(tiles[0][2])
        tile4.text = String(tiles[0][3])
        tile5.text = String(tiles[1][0])
        tile6.text = String(tiles[1][1])
        tile7.text = String(tiles[1][2])
        tile8.text = String(tiles[1][3])
        tile9.text = String(tiles[2][0])
        tile10.text = String(tiles[2][1])
        tile11.text = String(tiles[2][2])
        tile12.text = String(tiles[2][3])
        tile13.text = String(tiles[3][0])
        tile14.text = String(tiles[3][1])
        tile15.text = String(tiles[3][2])
        tile16.text = String(tiles[3][3])
    }
    
    func updateBoard(tileArray: [[Int]], direction:String){
        switch direction{
          case "up": tilesUp(tileArray)
          //case "down": tilesDown(tiles)
          //case "right": tilesRight(tiles)
         // case "left": tilesLeft(tiles)
        }
    }
    
    func tilesUp(tiles: [[Int]]){
        for i in tiles{
            for j in tiles{
                if(j == (j+1)){
                    j *= 2
                }
            }
        }
    }

}

